module gitlab.com/coalang/compiler

go 1.14

require (
	github.com/antlr/antlr4 v0.0.0-20200712162734-eb1adaa8a7a6
	github.com/webview/webview v0.0.0-20200724072439-e0c01595b361 // indirect
)
