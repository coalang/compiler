package main

import (
	"fmt"
	"log"
)

// gen_cgp - generate_coa_go_program

func (p Program) gen(trace []string) string {
	return fmt.Sprintf(
		"// Generated by Coa v0.0.0-a\npackage main\nimport \"gitlab.com/coalang/go-core\"\nfunc main() {%s.exec()}", p.blk.gen(trace))
}

func (b Block) gen(trace []string) string {
	source := "coa.NewBlock("
	for i, o := range b {
		source += o.gen(append(trace, fmt.Sprintf("block %v", i)))
		source += ","
	}
	source += ")"
	return source
}

func (m Map) gen(trace []string) string {
	trace = append(trace, fmt.Sprintf("object map"))
	log.Printf("Trace: %s", trace)
	log.Print("Map")
	var resultFmt = "coa.NewMap(map[coa.NewRef]coa.NewObject{%s})"
	var argFmt = "%s: %s"
	var args = ""
	for id, obj := range m {
		args += fmt.Sprintf(argFmt, id.gen(trace), obj.gen(trace))
	}
	return fmt.Sprintf(resultFmt, args)
}

func (i Id) gen(trace []string) string {
	trace = append(trace, "object id")
	log.Printf("Trace: %s", trace)
	log.Print("Id (converted to Ref)")
	var resultFmt = "coa.NewRef(\"%s\")"
	idRune := []rune(i.str)
	return fmt.Sprintf(resultFmt, string(idRune[1:len(idRune)-1]))
}

func (o Object) gen(trace []string) string {
	trace = append(trace, fmt.Sprintf("object %s", o.typeName))
	log.Printf("Trace: %s", trace)
	switch o.typeName {
	case "ref":
		log.Print("Ref")
		var resultFmt = "coa.NewRef(\"%s\")"
		id := fmt.Sprintf("%s", o.id)
		idRune := []rune(id)
		return fmt.Sprintf(resultFmt, string(idRune[1:len(idRune)-1]))
	case "call":
		log.Print("Call")
		var resultFmt = "coa.NewCall(coa.NewRef(\"%s\"), coa.NewMap(\"%s\"))"
		id := fmt.Sprintf("%s", o.id)
		idRune := []rune(id)
		return fmt.Sprintf(resultFmt, string(idRune[1:len(idRune)-1]), o.mp)
	case "text":
		log.Print("Text")
		var resultFmt = "coa.NewText(coa.NewRawText([]string{%s}), coa.NewFmtText(%s))"
		rawTextArgs := ""
		fmtArgs := ""
		for _, txt := range o.txts {
			rawTextArgs += fmt.Sprintf("\"%s\",", txt)
		}
		for _, fmt_ := range o.fmts {
			fmtArgs += fmt_.gen(trace)
		}
		return fmt.Sprintf(resultFmt, rawTextArgs, fmtArgs)
	case "function":
		log.Print("Function")
		var resultFmt = "coa.NewFunction(%s, %s)"
		var oBlkGen string
		if o.blk == nil {
			log.Panicf("Block of function %s is nil", trace)
		} else if len(o.blk) == 0 {
			oBlkGen = "nil"
		} else {
			oBlkGen = o.blk.gen(trace)
		}
		return fmt.Sprintf(resultFmt, o.mp.gen(trace), oBlkGen)
	default:
		log.Panicf("Object of block %s has unexpected type: %s", trace, o.typeName)
		return "panic"
	}
}
