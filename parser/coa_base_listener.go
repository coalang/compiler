// Code generated from Coa.g4 by ANTLR 4.8. DO NOT EDIT.

package parser // Coa

import "github.com/antlr/antlr4/runtime/Go/antlr"

// BaseCoaListener is a complete listener for a parse tree produced by CoaParser.
type BaseCoaListener struct{}

var _ CoaListener = &BaseCoaListener{}

// VisitTerminal is called when a terminal node is visited.
func (s *BaseCoaListener) VisitTerminal(node antlr.TerminalNode) {}

// VisitErrorNode is called when an error node is visited.
func (s *BaseCoaListener) VisitErrorNode(node antlr.ErrorNode) {}

// EnterEveryRule is called when any rule is entered.
func (s *BaseCoaListener) EnterEveryRule(ctx antlr.ParserRuleContext) {}

// ExitEveryRule is called when any rule is exited.
func (s *BaseCoaListener) ExitEveryRule(ctx antlr.ParserRuleContext) {}

// EnterStart is called when production start is entered.
func (s *BaseCoaListener) EnterStart(ctx *StartContext) {}

// ExitStart is called when production start is exited.
func (s *BaseCoaListener) ExitStart(ctx *StartContext) {}

// EnterBlock is called when production block is entered.
func (s *BaseCoaListener) EnterBlock(ctx *BlockContext) {}

// ExitBlock is called when production block is exited.
func (s *BaseCoaListener) ExitBlock(ctx *BlockContext) {}

// EnterRawBlock is called when production rawBlock is entered.
func (s *BaseCoaListener) EnterRawBlock(ctx *RawBlockContext) {}

// ExitRawBlock is called when production rawBlock is exited.
func (s *BaseCoaListener) ExitRawBlock(ctx *RawBlockContext) {}

// EnterRawBlockNonNil is called when production rawBlockNonNil is entered.
func (s *BaseCoaListener) EnterRawBlockNonNil(ctx *RawBlockNonNilContext) {}

// ExitRawBlockNonNil is called when production rawBlockNonNil is exited.
func (s *BaseCoaListener) ExitRawBlockNonNil(ctx *RawBlockNonNilContext) {}

// EnterObject is called when production object is entered.
func (s *BaseCoaListener) EnterObject(ctx *ObjectContext) {}

// ExitObject is called when production object is exited.
func (s *BaseCoaListener) ExitObject(ctx *ObjectContext) {}

// EnterObjectDivider is called when production objectDivider is entered.
func (s *BaseCoaListener) EnterObjectDivider(ctx *ObjectDividerContext) {}

// ExitObjectDivider is called when production objectDivider is exited.
func (s *BaseCoaListener) ExitObjectDivider(ctx *ObjectDividerContext) {}

// EnterRef is called when production ref is entered.
func (s *BaseCoaListener) EnterRef(ctx *RefContext) {}

// ExitRef is called when production ref is exited.
func (s *BaseCoaListener) ExitRef(ctx *RefContext) {}

// EnterRefInside is called when production refInside is entered.
func (s *BaseCoaListener) EnterRefInside(ctx *RefInsideContext) {}

// ExitRefInside is called when production refInside is exited.
func (s *BaseCoaListener) ExitRefInside(ctx *RefInsideContext) {}

// EnterRefInsideNoDot is called when production refInsideNoDot is entered.
func (s *BaseCoaListener) EnterRefInsideNoDot(ctx *RefInsideNoDotContext) {}

// ExitRefInsideNoDot is called when production refInsideNoDot is exited.
func (s *BaseCoaListener) ExitRefInsideNoDot(ctx *RefInsideNoDotContext) {}

// EnterText is called when production text is entered.
func (s *BaseCoaListener) EnterText(ctx *TextContext) {}

// ExitText is called when production text is exited.
func (s *BaseCoaListener) ExitText(ctx *TextContext) {}

// EnterTextNonNil is called when production textNonNil is entered.
func (s *BaseCoaListener) EnterTextNonNil(ctx *TextNonNilContext) {}

// ExitTextNonNil is called when production textNonNil is exited.
func (s *BaseCoaListener) ExitTextNonNil(ctx *TextNonNilContext) {}

// EnterTextNil is called when production textNil is entered.
func (s *BaseCoaListener) EnterTextNil(ctx *TextNilContext) {}

// ExitTextNil is called when production textNil is exited.
func (s *BaseCoaListener) ExitTextNil(ctx *TextNilContext) {}

// EnterTextFmted is called when production textFmted is entered.
func (s *BaseCoaListener) EnterTextFmted(ctx *TextFmtedContext) {}

// ExitTextFmted is called when production textFmted is exited.
func (s *BaseCoaListener) ExitTextFmted(ctx *TextFmtedContext) {}

// EnterIdFmted is called when production idFmted is entered.
func (s *BaseCoaListener) EnterIdFmted(ctx *IdFmtedContext) {}

// ExitIdFmted is called when production idFmted is exited.
func (s *BaseCoaListener) ExitIdFmted(ctx *IdFmtedContext) {}

// EnterFmtText is called when production fmtText is entered.
func (s *BaseCoaListener) EnterFmtText(ctx *FmtTextContext) {}

// ExitFmtText is called when production fmtText is exited.
func (s *BaseCoaListener) ExitFmtText(ctx *FmtTextContext) {}

// EnterFunction is called when production function is entered.
func (s *BaseCoaListener) EnterFunction(ctx *FunctionContext) {}

// ExitFunction is called when production function is exited.
func (s *BaseCoaListener) ExitFunction(ctx *FunctionContext) {}

// EnterCall is called when production call is entered.
func (s *BaseCoaListener) EnterCall(ctx *CallContext) {}

// ExitCall is called when production call is exited.
func (s *BaseCoaListener) ExitCall(ctx *CallContext) {}

// EnterMapping is called when production mapping is entered.
func (s *BaseCoaListener) EnterMapping(ctx *MappingContext) {}

// ExitMapping is called when production mapping is exited.
func (s *BaseCoaListener) ExitMapping(ctx *MappingContext) {}

// EnterMapNonNil is called when production mapNonNil is entered.
func (s *BaseCoaListener) EnterMapNonNil(ctx *MapNonNilContext) {}

// ExitMapNonNil is called when production mapNonNil is exited.
func (s *BaseCoaListener) ExitMapNonNil(ctx *MapNonNilContext) {}

// EnterObjectSpace is called when production objectSpace is entered.
func (s *BaseCoaListener) EnterObjectSpace(ctx *ObjectSpaceContext) {}

// ExitObjectSpace is called when production objectSpace is exited.
func (s *BaseCoaListener) ExitObjectSpace(ctx *ObjectSpaceContext) {}

// EnterComment is called when production comment is entered.
func (s *BaseCoaListener) EnterComment(ctx *CommentContext) {}

// ExitComment is called when production comment is exited.
func (s *BaseCoaListener) ExitComment(ctx *CommentContext) {}
