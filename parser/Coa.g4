grammar Coa;

start
 : rb=rawBlock
 ;

block
 : OBRACK rb=rawBlock CBRACK
 ;

rawBlock
 : rbnnil=rawBlockNonNil?
 ;

rawBlockNonNil
 : objdiv=objectDivider* obj=object
 ;

object
 : rf=ref
 | txt=text
 | fmttxt=fmtText
 | fun=function
 | cll=call
 | mp=mapping
 | cmt=comment
 ;

objectDivider
 : (rf=ref SPACE)
 | (txt=text SPACE)
 | (fmttxt=fmtText SPACE)
 | (fun=function SPACE)
 | (cll=call SPACE)
 | (mp=mapping SPACE)
 | (cmt=comment NEWLINE)
 ;

ref
 : rfins=refInside* rfinsndot=refInsideNoDot
 ;

refInside
 : idfmt=idFmted DOT
 ;

refInsideNoDot
 : idfmt=idFmted
 ;

text
 : txtnil=textNil | txtnnil=textNonNil
 ;

textNonNil
 : SINGLEQUOTE txtfmt=textFmted SINGLEQUOTE
 ;

textNil
 : SINGLEQUOTE SINGLEQUOTE
 ;

textFmted // TODO: Add support for non-alphabet & numbers.
 : (IDINSIDE ft=fmtText?)*
 ;

idFmted
 : (IDINSIDE ft=fmtText?)*
 ;

fmtText
 : DOUBLEQUOTE rb=rawBlock DOUBLEQUOTE
 ;

function
 : mp=mapping? blk=block
 ;

call
 : rf=ref mp=mapping
 ;

mapping
 : OPAREN mpnnil=mapNonNil? CPAREN
 ;

mapNonNil
 : objspc=objectSpace* obj=object
 ;

objectSpace
 : mpins=object SPACE
 ;

comment
 : NUMBERSIGN cmtins=.*?
 ;

OBRACK : '[' ;
CBRACK : ']' ;
OPAREN : '(' ;
CPAREN : ')' ;
SINGLEQUOTE : '\'' ;
DOUBLEQUOTE : '"' ;
DOT : '.' ;
ID : IDINSIDE{1,} ; // TODO: Add support for non-alphabet & numbers.
//IDINSIDE : ~['"[\]()#.\r\n\f\t ] ;
//IDINSIDE : ~[*] ;
//IDINSIDE : [!$-&*--/-Z\\^-~] ;
IDINSIDE : [a-zA-Z0-9] ;
NUMBERSIGN : '#' ;
NEWLINE : [\r\n\f] ;
SPACE : [\r\n\f\t ] ;
