package main

import (
	"fmt"
	"github.com/antlr/antlr4/runtime/Go/antlr"
	"gitlab.com/coalang/compiler/parser"
)

type coaListener struct {
	*parser.BaseCoaListener
	stack []interface{}
}

//func (l *coaListener) VisitTerminal(node antlr.TerminalNode) {
//	panic("implement me")
//}
//
//func (l *coaListener) VisitErrorNode(node antlr.ErrorNode) {
//	panic("implement me")
//}
//
//func (l *coaListener) EnterEveryRule(ctx antlr.ParserRuleContext) {
//	panic("implement me")
//}
//
//func (l *coaListener) ExitEveryRule(ctx antlr.ParserRuleContext) {
//	panic("implement me")
//}

func (l *coaListener) push(i interface{}) {
	fmt.Println("push", l.stack)
	l.stack = append(l.stack, i)
	fmt.Println("pushed", l.stack)
}

func (l *coaListener) poppable() bool {
	return len(l.stack) > 0
}

func (l *coaListener) pop() interface{} {
	fmt.Println("pop", l.stack)
	if !l.poppable() {
		panic("stack is empty unable to pop")
	}

	// Get the last value from the stack.
	result := l.stack[len(l.stack)-1]

	// Pop the last element from the stack.
	l.stack = l.stack[:len(l.stack)-1]

	return result
}

//func (l *coaListener) ExitCall(c *CallContext) {
//	//if l.poppable() {
//	//	 := l.pop()
//	//} else {
//	//
//	//}
//	rf := l.pop()
//	mp := l.pop()
//	//rf := c.Ref()
//	//mp := c.Map()
//	fmt.Println("Ref: ", rf)
//	fmt.Println("Map: ", mp)
//	l.push(Call{
//		rf: rf,
//		mp: mp
//	})
//}

func (l *coaListener) ExitStart(c *parser.StartContext) {
	rb := c.GetRb()
	fmt.Println(rb)
	processRawBlock(rb)
}

//// ExitMulDiv is called when exiting the MulDiv production.
//func (l *coaListener) ExitMulDiv(c *MulDivContext) {
//	right, left := l.pop(), l.pop()
//
//	switch c.GetOp().GetTokenType() {
//	case CalcParserMUL:
//		l.push(left * right)
//	case CalcParserDIV:
//		l.push(left / right)
//	default:
//		panic(fmt.Sprintf("unexpected operation: %s", c.GetOp().GetText()))
//	}
//}
//
//// ExitExpRot is called when exiting the ExpRot production.
//func (l *coaListener) ExitExpRot(c *ExpRotContext) {
//	right, left := l.pop(), l.pop()
//
//	switch c.GetOp().GetTokenType() {
//	case CalcParserEXP:
//		l.push(math.Pow(left, right))
//	case CalcParserROT:
//		l.push(math.Pow(left, 1.0/right)) // sqrt(a) = a**(1.0/2.0)
//	default:
//		panic(fmt.Sprintf("unexpected operation: %s", c.GetOp().GetText()))
//	}
//}
//
//// ExitAddSub is called when exiting the AddSub production.
//func (l *coaListener) ExitAddSub(c *AddSubContext) {
//	right, left := l.pop(), l.pop()
//
//	switch c.GetOp().GetTokenType() {
//	case CalcParserADD:
//		l.push(left + right)
//	case CalcParserSUB:
//		l.push(left - right)
//	default:
//		panic(fmt.Sprintf("unexpected operation: %s", c.GetOp().GetText()))
//	}
//}
//
//// ExitInteger is called when exiting the Integer production.
//func (l *coaListener) ExitInteger(c *IntegerContext) {
//	i, err := strconv.ParseFloat(c.GetText(), 64)
//	if err != nil {
//		panic(err.Error())
//	}
//
//	l.push(float64(i))
//}
//
//// ExitFloat is called when exiting the Float production.
//func (l *coaListener) ExitFloat(c *FloatContext) {
//	i, err := strconv.ParseFloat(c.GetText(), 64)
//	if err != nil {
//		fmt.Println(err)
//		panic(err.Error())
//	}
//
//	l.push(float64(i))
//}

// eval takes a string expression and returns the evaluated result.
func eval(input string) interface{} {
	// Setup the input
	inputStream := antlr.NewInputStream(input)

	// Create the Lexer
	streamLexer := parser.NewCoaLexer(inputStream)
	tokenStream := antlr.NewCommonTokenStream(streamLexer, antlr.TokenDefaultChannel)

	// Create the Parser
	streamParser := parser.NewCoaParser(tokenStream)

	// Finally parse the expression (by walking the tree)
	var listener coaListener
	antlr.ParseTreeWalkerDefault.Walk(&listener, streamParser.Start())

	//return listener.pop()
	return nil
}
