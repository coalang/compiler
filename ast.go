package main

import (
	"fmt"
	"gitlab.com/coalang/compiler/parser"
	//"github.com/antlr/antlr4/runtime/Go/antlr"
	//"gitlab.com/coalang/compiler/parser"
)

//type RawBlockContext struct {
//	*antlr.BaseParserRuleContext
//	parser antlr.Parser
//	rbnnil IRawBlockNonNilContext
//}

type Program struct {
	blk Block
}

type Id struct {
	str string // TODO: It's kinda not intuitive, but fix it later.
}

type Block []Object

func processRawBlock(rb parser.IRawBlockContext) Block {
	rbnnil := rb.GetRbnnil()
	obj := rbnnil.GetObj()
	objDiv := rbnnil.GetObjdiv()
	objs := make([]Object, 0)
	objs = append(objs, processObj(obj))
	objs = append(objs, processObjDiv(objDiv)...)
	fmt.Println("obj", obj)
	fmt.Println("objDiv", objDiv)
	//: rf=ref
	//| txt=text
	//| fmttxt=fmtText
	//| fun=function
	//| cll=call
	//| mp=mapping
	//| cmt=comment
	//for i := 0; i < len(objDiv) + 1; i ++ {
	//
	//}
	return nil
}

func processObj(obj parser.IObjectContext) Object {
	return Object{
		typeName: "",
		id:       Id{""},
		txts:     nil,
		fmts:     nil,
		mp:       nil,
		blk:      nil,
	}
}

func processObjDiv(objDiv parser.IObjectDividerContext) []Object {
	objs := make([]Object, 0)
	//for i := 0; i < len(objDiv); i ++ {
	//	objs = append(objs, processObj(objDiv[i]))
	//}
	objs = append(objs, Object{
		typeName: "",
		id:       Id{""},
		txts:     nil,
		fmts:     nil,
		mp:       nil,
		blk:      nil,
	})
	return objs
}

type Object struct {
	typeName string
	id       Id       // Ref, Call
	txts     []string // Text
	fmts     []Block  // Text
	mp       Map      // Function, Call, Map
	blk      Block    // Function, Block
}

type Map map[Id]Object

type Comment string
