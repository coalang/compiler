package main

import (
	"fmt"
	"testing"
)

func TestGen(t *testing.T) {
	test0Program := Program{}
	test0Program.blk = append(test0Program.blk, Object{
		typeName: "ref",
		id:       Id{"a.b"},
		txts:     nil,
		fmts:     nil,
		mp:       nil,
		blk:      nil,
	})
	test0Program.blk = append(test0Program.blk, Object{
		typeName: "call",
		id:       Id{"a.b"},
		txts:     nil,
		fmts:     nil,
		mp:       Map{},
		blk:      nil,
	})
	test0Program.blk = append(test0Program.blk, Object{
		typeName: "text",
		id:       Id{""},
		txts:     []string{"some text lul"},
		fmts:     []Block{},
		mp:       nil,
		blk:      nil,
	})
	test0Program.blk = append(test0Program.blk, Object{
		typeName: "function",
		id:       Id{""},
		txts:     nil,
		fmts:     nil,
		mp: Map{Id{"key"}: Object{
			typeName: "ref",
			id:       Id{"value"},
			txts:     nil,
			fmts:     nil,
			mp:       nil,
			blk:      nil,
		}},
		blk: Block{},
	})
	fmt.Printf("%s\n", test0Program.gen(make([]string, 0)))
}
