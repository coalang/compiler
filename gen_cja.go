package main

import (
	"encoding/json"
	"fmt"
	"log"
)

// gen_cja - generate_coa_json_ast

type CjaObject interface {
	IsCjaObject()
}

type CjaProgram struct {
	Meta  map[string]string `json:"meta"`
	Block CjaBlock          `json:"block"`
}

func (p CjaProgram) IsCjaObject() {}

func (p CjaProgram) _genJson(indent bool) (string, error) {
	var jsonStr []byte
	var err error
	if indent {
		jsonStr, err = json.MarshalIndent(p, "", "  ")
	} else {
		jsonStr, err = json.Marshal(p)
	}
	if err != nil {
		return "", err
	}
	return string(jsonStr), nil
}

func (p CjaProgram) genJson() (string, error) {
	return p._genJson(false)
}

func (p CjaProgram) genJsonIndent() (string, error) {
	return p._genJson(true)
}

type CjaBlock struct {
	Interface CjaMap      `json:"inter"`
	Content   []CjaObject `json:"cont"`
}

func (b CjaBlock) IsCjaObject() {}

type CjaMap []CjaMapItem

func (m CjaMap) IsCjaObject() {}

type CjaMapItem struct {
	Key   CjaId     `json:"key"`
	Order int       `json:"order"`
	Value CjaObject `json:"val"`
}

type CjaCall struct {
	Callee CjaId  `json:"callee"`
	Args   CjaMap `json:"args"`
}

func (c CjaCall) IsCjaObject() {}

type CjaId string

func (i CjaId) IsCjaObject() {}

type CjaText struct { // TODO: Replace with `CjaMap` of `CjaChar`s and `CjaBlock`s
	Txts []string   `json:"txts"`
	Fmts []CjaBlock `json:"fmts"`
}

func (t CjaText) IsCjaObject() {}

type CjaComment string

func (c CjaComment) IsCjaObject() {}

type CjaBlank struct{}

func (b CjaBlank) IsCjaObject() {}

func (p Program) genCja(trace []string) CjaProgram {
	return CjaProgram{
		Meta: map[string]string{
			"ver": verInfo.text,
		},
		Block: p.blk.genCja(append(trace, "block")),
	}
}

func (b Block) genCja(trace []string) CjaBlock {
	var content []CjaObject
	for i, o := range b {
		content = append(content, o.genCja(append(trace, fmt.Sprintf("content-%v", i))))
	}
	return CjaBlock{
		Interface: nil,
		Content:   content,
	}
}

func (m Map) genCja(trace []string) CjaMap {
	var mapItems CjaMap
	var i = 0
	for key, value := range m {
		mapItems = append(mapItems, CjaMapItem{
			Key:   key.genCja(append(trace, "mapItem")),
			Order: i,
			Value: value.genCja(append(trace, "mapItem")),
		})
		i++
	}
	return mapItems
}

func (i Id) genCja(_ []string) CjaId {
	return CjaId(i.str)
}

func (o Object) genCja(trace []string) CjaObject {
	switch o.typeName {
	case "ref":
		return o.id.genCja(append(trace, "id"))
	case "call":
		return CjaCall{
			Callee: o.id.genCja(append(trace, "id")),
			Args:   o.mp.genCja(append(trace, "maps")),
		}
	case "text":
		var fmts []CjaBlock
		for i, v := range o.fmts {
			fmts = append(fmts, v.genCja(append(trace, fmt.Sprintf("fmt-%v", i))))
		}
		return CjaText{
			Txts: o.txts,
			Fmts: fmts,
		}
	case "block":
		block := o.blk.genCja(append(trace, "block"))
		block.Interface = o.mp.genCja(append(trace, "inter"))
		return block
	default:
		log.Panicf("Object of block %s has unexpected type: %s", trace, o.typeName)
		return CjaBlank{}
	}
}

func (c Comment) genCja(_ []string) CjaComment {
	return CjaComment(c)
}
