// example1.go
package main

//import (
//	"bufio"
//	"fmt"
//	"github.com/antlr/antlr4/runtime/Go/antlr"
//	"gitlab.com/coalang/compiler/parser"
//	"log"
//	"os"
//	"runtime/debug"
//)
//
//func singleRepl(prompt string, reader bufio.Reader) bool {
//	defer func() {
//		if r := recover(); r != nil {
//			fmt.Println("x Unrecoverable error occurred. Call stack:")
//			debug.PrintStack()
//		}
//	}()
//	log.Print(prompt)
//	inputString, _ := reader.ReadString('\n')
//	if inputString == "exit" {
//		return false
//	}
//	inputStream := antlr.NewInputStream(inputString)
//	inputLexer := NewCoaLexer(inputStream)
//	for {
//		currentToken := inputLexer.NextToken()
//		if currentToken.GetTokenType() == antlr.TokenEOF {
//			break
//		}
//		//if inputLexer.SymbolicNames[currentToken.GetTokenType()] == "ID" {
//		//	continue
//		//}
//		// .out('Hello, "..dev.coa.core.sys.user.name"!')
//		log.Printf("%s (%q)\n", inputLexer.SymbolicNames[currentToken.GetTokenType()], currentToken.GetText())
//	}
//	return true
//}
//
//func repl() {
//	reader := bufio.NewReader(os.Stdin)
//	prompt := "Input:"
//	contRepl := true
//	for contRepl {
//		contRepl = singleRepl(prompt, *reader)
//	}
//}
//
//
//func main() {
//	log.Println("Coa Lexer Tester")
//	repl()
//}
