package main

import (
	"bufio"
	"fmt"
	"os"
	"runtime/debug"
)

func repl(prompt string) {
	fmt.Println("i Coa REPL")
	reader := bufio.NewReader(os.Stdin)
	var exit_repl bool = false
	for !exit_repl {
		exit_repl = singleRepl(prompt, *reader)
	}
}

func singleRepl(prompt string, reader bufio.Reader) bool {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("x Unrecoverable error occurred. Call stack:")
			debug.PrintStack()
		}
	}()
	var exit_repl bool = false
	var out interface{}
	fmt.Print(prompt)
	text, _ := reader.ReadString('\n')
	if text == "exit_repl" {
		exit_repl = true
	}
	out = eval(text)
	fmt.Printf("✓ %v\n", out)
	return exit_repl
}
