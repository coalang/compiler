package main

import "fmt"

var verInfo = struct {
	nums    [3]int
	channel string
	text    string
}{
	nums:    [3]int{0, 0, 0},
	channel: "alpha",
	text:    fmt.Sprintf("%v.%v.%v-%s", 0, 0, 0, "alpha"),
}
