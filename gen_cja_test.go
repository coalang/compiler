package main

import (
	"fmt"
	"testing"
)

func TestGenCja(t *testing.T) {
	test0Program := Program{}
	test0Program.blk = append(test0Program.blk, Object{
		typeName: "ref",
		id:       Id{"a.b"},
		txts:     nil,
		fmts:     nil,
		mp:       nil,
		blk:      nil,
	})
	test0Program.blk = append(test0Program.blk, Object{
		typeName: "call",
		id:       Id{"a.b"},
		txts:     nil,
		fmts:     nil,
		mp:       Map{},
		blk:      nil,
	})
	test0Program.blk = append(test0Program.blk, Object{
		typeName: "text",
		id:       Id{""},
		txts:     []string{"some text lul"},
		fmts:     []Block{},
		mp:       nil,
		blk:      nil,
	})
	test0Program.blk = append(test0Program.blk, Object{
		typeName: "block",
		id:       Id{""},
		txts:     nil,
		fmts:     nil,
		mp:       nil,
		blk:      Block{},
	})
	jsonStr, err := test0Program.genCja(make([]string, 0)).genJsonIndent()
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\n", jsonStr)
}
